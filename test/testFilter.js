const filter = require('../filter.js')

//1.
let arr = [1,2,3,4,5,5]
let cb = function(x,i){return x>1}
let result = filter(arr,cb)
console.log(result)

//2. function that dosent return true or false
arr = [1,2,3,4,5,5]
cb = function(x){ return x>2}
result = filter(arr,cb)
console.log(result)

//3. Empty array
arr = []
cb = function(x,i){return true}
result = filter(arr,cb)
console.log(result)

//4. Not an array
arr = 'abc'
cb = function(x,i,arr){return true}
result = filter(arr,cb)
console.log(result)

//5.
arr = [1,[2,3,4,],5,5]
cb = (x)=>x>2
result = filter(arr,cb)
console.log(result)