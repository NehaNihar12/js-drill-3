const flatten = require('../flatten.js');

//1.
let arr = [1, [2], [[3]], [[[4]]]];
let result = flatten(arr);
console.log("1. " + result)

//2. arr=[]
arr = []
result = flatten(arr);
console.log("2. " + result)

//3.ar=[1,2,3]
arr = [1, 2, 3]
result = flatten(arr);
console.log("3. " + result)

//3. arr= {'a':2,'b':3,'c':4}
arr = { 'a': 2, 'b': 3, 'c': 4 }
result = flatten(arr);
console.log("4. " + result)
