function reduce(arr,cb,startingValue){

    if(!arr || !cb){
        return undefined;
    }

    let i=0;

    if(!startingValue){
        startingValue = arr[0];
        i=1;
    } 
    
    for(;i<arr.length;i++){
        startingValue = cb(startingValue,arr[i],arr)
    }
    return startingValue;
}

module.exports = reduce;