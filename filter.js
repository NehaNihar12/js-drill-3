function filter(arr,cb){

    if(!arr || !cb){
        return []
    }
    let newarr = [];
    
    for(let i=0;i<arr.length;i++){
        if(cb(arr[i],i,arr)===true){
            newarr.push(arr[i]);
        }
    }
    return newarr;
}

module.exports = filter